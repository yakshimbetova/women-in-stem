//
//  MainBandView.m
//  Women in STEM
//
//  Created by Macbook on 10/5/15.
//  Copyright © 2015 Techwomen. All rights reserved.
//

#import "MainBandView.h"
#import "Flurry.h"
#define TITLE_VIEW_HEIGHT   40
#define GAP                 5
#define BTN_SIZE            44
#import "Defines.h"

@implementation MainBandView
@synthesize lblDesc, ivImage;
@synthesize titleView, titleImage, lblTitle;
@dynamic mainImage;
@synthesize delegate;
-(id) initWithFrame:(CGRect) frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, TITLE_VIEW_HEIGHT)];
        titleView.backgroundColor = Rgb2UIColor(252, 244, 240);
        [self addSubview:titleView];
        titleImage = [[UIImageView alloc] initWithFrame:CGRectMake(GAP/2.0, GAP/2.0, TITLE_VIEW_HEIGHT, TITLE_VIEW_HEIGHT-GAP)];
        titleImage.contentMode = UIViewContentModeScaleAspectFill;
        titleImage.backgroundColor = [UIColor clearColor];
        [titleView addSubview:titleImage];
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(titleImage.frame) + GAP, GAP, frame.size.width - 2*GAP - CGRectGetMaxX(titleImage.frame), 10000)];
        lblTitle.backgroundColor = [UIColor clearColor];
        lblTitle.font = FONT_TITLE;
        lblTitle.numberOfLines = 0;
        lblTitle.textColor = Rgb2UIColor(6, 43, 89);;
        [titleView addSubview:lblTitle];

        ivImage = [[UIImageView alloc] initWithFrame:CGRectMake(GAP, CGRectGetMaxY(titleView.frame)+GAP, 0, 0)];
        ivImage.backgroundColor = [UIColor clearColor];
        ivImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:ivImage];

        
        lblDesc = [[UILabel alloc] initWithFrame:CGRectMake(GAP, CGRectGetMaxY(ivImage.frame) + GAP, frame.size.width - 2*GAP, 50)];
        lblDesc.backgroundColor = [UIColor clearColor];
        lblDesc.numberOfLines = 2;
        lblDesc.lineBreakMode = NSLineBreakByTruncatingTail;
        lblDesc.font = FONT_DESCRIPTION;
        lblDesc.textColor = [UIColor blackColor];
        [self addSubview:lblDesc];

        
        // share and like part
        btnShare = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width/4.0 - BTN_SIZE/2.0, CGRectGetMaxY(ivImage.frame) + GAP, BTN_SIZE, BTN_SIZE)];
        btnShare.backgroundColor = [UIColor clearColor];
        [btnShare setImage:[UIImage imageNamed:@"ic_share"] forState:UIControlStateNormal];
        [btnShare addTarget:self action:@selector(doShare:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btnShare];

        btnLike = [[UIButton alloc] initWithFrame:CGRectMake(3.0*self.frame.size.width/4.0 - BTN_SIZE/2.0, CGRectGetMaxY(ivImage.frame) + GAP, BTN_SIZE, BTN_SIZE)];
        btnLike.backgroundColor = [UIColor clearColor];
        [btnLike setImage:[UIImage imageNamed:@"ic_like"] forState:UIControlStateNormal];
        [btnLike addTarget:self action:@selector(doLike:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btnLike];
        
        
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, CGRectGetMaxY(btnLike.frame)+GAP);
    }
    return self;
}

-(id) initWithFrame:(CGRect)frame andModel:(NewsModel *) newsmodel
{
    self = [self initWithFrame:frame];
    if (self) {
        curModel = newsmodel;
        lblTitle.text = newsmodel.categoryTitle;
        [lblTitle sizeToFit];
        lblTitle.frame = CGRectMake(CGRectGetMaxX(titleImage.frame) + GAP, (titleView.frame.size.height - lblTitle.frame.size.height) / 2.0, lblTitle.frame.size.width, lblTitle.frame.size.height);
        titleImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:newsmodel.categoryImage]]];
        lblDesc.frame = CGRectMake(GAP, CGRectGetMaxY(ivImage.frame) + GAP, frame.size.width - 2*GAP, 50);
        lblDesc.numberOfLines = 2;
        lblDesc.text = newsmodel.desc;
        lblDesc.lineBreakMode = NSLineBreakByTruncatingTail;
        
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, CGRectGetMaxY(lblDesc.frame)+GAP);

    }
    return self;
}


-(void) setMainImage:(UIImage *)mainImage
{
    if (mainImage) {
        ivImage.image = mainImage;
        curModel.image = mainImage;
        ivImage.frame = CGRectMake(GAP, CGRectGetMaxY(titleView.frame) + GAP, self.frame.size.width - 2*GAP, mainImage.size.height*(self.frame.size.width - 2*GAP)/mainImage.size.width);
        lblDesc.frame = CGRectMake(GAP, CGRectGetMaxY(ivImage.frame) + GAP, lblDesc.frame.size.width, lblDesc.frame.size.height );
        btnShare.frame = CGRectMake(self.frame.size.width/4.0 - BTN_SIZE/2.0, CGRectGetMaxY(lblDesc.frame) + GAP, BTN_SIZE, BTN_SIZE);
        btnLike.frame = CGRectMake(3.0*self.frame.size.width/4.0 - BTN_SIZE/2.0, CGRectGetMaxY(lblDesc.frame) + GAP, BTN_SIZE, BTN_SIZE);
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, CGRectGetMaxY(btnLike.frame)+GAP);
        
    }
    else
    {
        ivImage.frame = CGRectZero;
        lblDesc.frame = CGRectMake(GAP, CGRectGetMaxY(titleView.frame) + GAP, lblDesc.frame.size.width, lblDesc.frame.size.height );
        btnShare.frame = CGRectMake(self.frame.size.width/4.0 - BTN_SIZE/2.0, CGRectGetMaxY(lblDesc.frame) + GAP, BTN_SIZE, BTN_SIZE);
        btnLike.frame = CGRectMake(3.0*self.frame.size.width/4.0 - BTN_SIZE/2.0, CGRectGetMaxY(lblDesc.frame) + GAP, BTN_SIZE, BTN_SIZE);
        
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, CGRectGetMaxY(btnLike.frame)+GAP);
    
    }
}

-(UIImage *) mainImage
{
    return ivImage.image;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    CGPoint point = [((UITouch *) [touches anyObject]) locationInView:self];
    if(CGRectContainsPoint(self.bounds, point))
    {
        if (CGRectContainsPoint(ivImage.frame, point)) {
            if ([delegate respondsToSelector:@selector(touchInImageView:model:)]) {
                [delegate performSelector:@selector(touchInImageView:model:) withObject:self withObject:curModel];
            }
        }
        else
        {
            if([delegate respondsToSelector:@selector(touchInMainBandView:model:)])
                [delegate performSelector:@selector(touchInMainBandView:model:) withObject:self withObject:curModel];
        }
    }
    [super touchesEnded:touches withEvent:event];
}


-(void) doShare:(UIButton*) sender
{
    [Flurry logEvent:@"do_share_main_band"];
    if ([delegate respondsToSelector:@selector(doShareInMainBandView:model:)]) {
        [delegate performSelector:@selector(doShareInMainBandView:model:) withObject:self withObject:curModel];
    }
    
}

-(void) doLike:(UIButton*) sender
{
    [Flurry logEvent:@"do_like_main_band"];
    if ([delegate respondsToSelector:@selector(doLikeInMainBandView:model:)]) {
        [delegate performSelector:@selector(doLikeInMainBandView:model:) withObject:self withObject:curModel];
    }
    
}
@end
