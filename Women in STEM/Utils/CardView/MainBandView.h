//
//  MainBandView.h
//  Women in STEM
//
//  Created by Macbook on 10/5/15.
//  Copyright © 2015 Techwomen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsModel.h"
@class MainBandView;
@protocol MainBandViewProtocol <NSObject>
-(void) touchInMainBandView:(MainBandView*) view model:(NewsModel*) model;
-(void) touchInImageView:(MainBandView*) view model:(NewsModel*) model;
-(void) doShareInMainBandView:(MainBandView*) view model:(NewsModel*) model;
-(void) doLikeInMainBandView:(MainBandView*) view model:(NewsModel*) model;
@end

@interface MainBandView : UIView
{
    UIView *titleView;
    UILabel *lblTitle;
    UIImageView *titleImage;
    UILabel *lblDesc;
    UIImageView *ivImage;
    UIButton *btnShare;
    UIButton *btnLike;
    NewsModel *curModel;
   id<MainBandViewProtocol> __unsafe_unretained delegate;
}
@property (nonatomic, assign) id<MainBandViewProtocol> __unsafe_unretained delegate;
@property (nonatomic, retain) UIView *titleView;
@property (nonatomic, retain) UILabel *lblTitle;
@property (nonatomic, retain) UIImageView *titleImage;
@property (nonatomic, retain) UILabel *lblDesc;
@property (nonatomic, retain) UIImageView *ivImage;
@property (nonatomic, retain) UIImage *mainImage;
-(id) initWithFrame:(CGRect)frame andModel:(NewsModel *) newsmodel;
@end
