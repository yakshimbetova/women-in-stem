



#import <Foundation/Foundation.h>
#define RELOAD_MAIN_BAND_NOTIFICATION   @"ReloadMainBandNotification"
// ------------------- AppSettings class --------------
@interface AppSettings : NSObject {
    NSString *userPrefsKey;
    NSMutableDictionary *preferences;
}

@property(nonatomic, copy) NSString *userPrefsKey;			// key in User Preferences Dictionary for application

+ (AppSettings *)applicationSettings;

- (id)initWithKey:(NSString *)prefsKey appDefaultPrefs:(NSDictionary *)appDefaultPrefs;

- (void)userPrefsRegisterDefaults:(NSDictionary *)appDefaults;
- (BOOL)loadSettings;
- (void)saveSettings;

@property(nonatomic, assign) BOOL techwomen;	// dynamic
@property(nonatomic, assign) BOOL techgirls;					// dynamic

- (BOOL)getBOOLValue:(NSString *)key;
- (void)setBOOLValue:(BOOL)value forKey:(NSString *)key;

@end
