//
//  NewsModel.h
//  Women in STEM
//
//  Created by Macbook on 10/5/15.
//  Copyright © 2015 Techwomen. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#define NM_TITLE     @"title"
#define NM_DESC      @"desc"
#define NM_URL       @"url"

@interface NewsModel : NSObject

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *desc;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, retain) NSString *date;
@property (nonatomic, retain) NSString *urlString;
@property (nonatomic, retain) NSData *imageData;
@property (nonatomic, retain) NSString *categoryTitle;
@property (nonatomic, retain) NSString *categoryImage;
-(id) initWithDictionary:(NSDictionary *) dict;
@end
