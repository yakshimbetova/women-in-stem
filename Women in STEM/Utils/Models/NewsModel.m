//
//  NewsModel.m
//  Women in STEM
//
//  Created by Macbook on 10/5/15.
//  Copyright © 2015 Techwomen. All rights reserved.
//

#import "NewsModel.h"
@implementation NewsModel
@synthesize title, desc, imageData;
-(id) initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if (self) {
        self.title = [dict objectForKey:NM_TITLE];
        self.desc = [dict objectForKey:NM_DESC];
        self.urlString = [dict objectForKey:NM_URL];
    }
    return self;
}
@end
