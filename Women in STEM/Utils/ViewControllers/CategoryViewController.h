//
//  CategoryViewController.h
//  Women in STEM
//
//  Created by Macbook on 10/5/15.
//  Copyright © 2015 Techwomen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *categories;
}
@property (weak, nonatomic) IBOutlet UITableView *tblCategories;

@end
