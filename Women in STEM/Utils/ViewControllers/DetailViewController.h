//
//  DetailViewController.h
//  Women in STEM
//
//  Created by Macbook on 10/6/15.
//  Copyright © 2015 Techwomen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsModel.h"
@interface DetailViewController : UIViewController <UIActionSheetDelegate, UIGestureRecognizerDelegate>
{
    UILabel *lblTitle;
    UITextView *lblDescription;
    UIImageView *ivImage;
    UIButton *btnShare;
    UIButton *btnLike;
    NewsModel *currentModel;
    NSString *shareURl;    
}
@property (weak, nonatomic) IBOutlet UIScrollView *sv;
@property (nonatomic, retain) NewsModel *currentModel;
@end
