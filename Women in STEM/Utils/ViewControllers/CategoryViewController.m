//
//  CategoryViewController.m
//  Women in STEM
//
//  Created by Macbook on 10/5/15.
//  Copyright © 2015 Techwomen. All rights reserved.
//

#import "CategoryViewController.h"
#import "CategoryCell.h"
#import "AppSettings.h"
#import "Flurry.h"
@interface CategoryViewController ()
{
    AppSettings *settings;
}
@end

@implementation CategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Categories";
    categories = [[NSMutableArray alloc] initWithObjects:@"TechWomen",@"The TechGirls", nil];
    settings = [AppSettings applicationSettings];
    // Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated
{
    [Flurry logEvent:@"Category_view"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ---- TableView delegates -------

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1.0;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [categories count];
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryCell *cell = (CategoryCell *)[tableView dequeueReusableCellWithIdentifier:@"CategoryCell"];
    cell.lblTitle.text = [categories objectAtIndex:indexPath.row];
    cell.ivCheck.image = [UIImage imageWithData:[NSData data]];
    if (indexPath.row == 0)
    {
        cell.tag = (settings.techwomen)?1:0;
        cell.ivImage.image = [UIImage imageNamed:@"techwomen.JPG"];
    }
    if (indexPath.row == 1)
    {
        cell.tag = (settings.techgirls)?1:0;
        cell.ivImage.image = [UIImage imageNamed:@"techgirls.jpg"];
    }
    if (cell.tag == 1) {
        cell.ivCheck.image = [UIImage imageNamed:@"ic_check"];
    }
    else
    {
        cell.ivCheck.image = [UIImage imageWithData:[NSData data]];
    }
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:RELOAD_MAIN_BAND_NOTIFICATION object:nil];
    CategoryCell *cell = (CategoryCell*)[tableView cellForRowAtIndexPath:indexPath];
    if (cell.tag == 0) {
        cell.tag = 1;
        cell.ivCheck.image = [UIImage imageNamed:@"ic_check"];
    }
    else
    {
        cell.tag = 0;
        cell.ivCheck.image = [UIImage imageWithData:[NSData data]];
    }
    if (indexPath.row == 0)
    {
        settings.techwomen = cell.tag;
    }
    if (indexPath.row == 1)
    {
        settings.techgirls = cell.tag;
    }
    
    [settings saveSettings];
}
@end
