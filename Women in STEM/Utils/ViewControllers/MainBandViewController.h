//
//  MainBandViewController.h
//  Women in STEM
//
//  Created by Macbook on 10/5/15.
//  Copyright © 2015 Techwomen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainBandView.h"
#import <CoreLocation/CoreLocation.h>

@interface MainBandViewController : UIViewController <MainBandViewProtocol, UIActionSheetDelegate>
{
    NSMutableArray *mainArray;
    NSString *shareURl;
}
@property (weak, nonatomic) IBOutlet UIScrollView *sv;
@end
