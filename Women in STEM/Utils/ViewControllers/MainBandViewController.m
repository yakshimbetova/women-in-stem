//
//  MainBandViewController.m
//  Women in STEM
//
//  Created by Macbook on 10/5/15.
//  Copyright © 2015 Techwomen. All rights reserved.
//
#import "STTwitterAPI.h"
#import "MainBandViewController.h"
#import "NewsModel.h"
#import "LoremIpsum.h"
#import "MBProgressHUD.h"
#import "DetailViewController.h"
#import "MFSideMenu.h"
#import "AppDelegate.h"
#import "AppSettings.h"
#import "Defines.h"
#import "CategoryViewController.h"
#import "GGFullScreenImageViewController.h"
#define MB_GAP      5.0
@implementation MainBandViewController
@synthesize sv;
-(void) viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Women in STEM";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationReceived:) name:RELOAD_MAIN_BAND_NOTIFICATION object:nil];

    sv.backgroundColor = [UIColor colorWithWhite:0.98 alpha:1.0];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_menu"] style:UIBarButtonItemStyleDone target:self action:@selector(showCategoriesView:)];
    self.navigationItem.rightBarButtonItem = item;
    mainArray = [[NSMutableArray alloc] initWithCapacity:0];

}

-(void) getLocationInformation
{
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locationManager requestAlwaysAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    CLLocation *location = locationManager.location;
    [Flurry setLatitude:location.coordinate.latitude
              longitude:location.coordinate.longitude
     horizontalAccuracy:location.horizontalAccuracy
       verticalAccuracy:location.verticalAccuracy];

}

- (void)notificationReceived:(NSNotification *)notification{
    NSString *name = notification.name;
    AppSettings *settings = [AppSettings applicationSettings];
    if(!name) return;
    else if (name && [name caseInsensitiveCompare:RELOAD_MAIN_BAND_NOTIFICATION] == NSOrderedSame)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        for (UIView *view in sv.subviews) {
            [view removeFromSuperview];
        }        
        mainArray = [[NSMutableArray alloc] initWithCapacity:0];
        if (settings.techwomen == YES) {
            [self getTweets:TECHWOMEN_TW];
        }
        if (settings.techgirls == YES) {
            [self getTweets:TECHGIRLS_TW];
        }

    }
    
}




-(void) viewDidAppear:(BOOL)animated
{
    [Flurry logEvent:@"Main_Band"];
    AppSettings *settings = [AppSettings applicationSettings];
    if ([sv.subviews count] <= 2) {
        if (settings.techwomen == YES) {
            [self getTweets:TECHWOMEN_TW];
        }
        if (settings.techgirls == YES) {
            [self getTweets:TECHGIRLS_TW];
        }

    }
//    else
//    {
//        [self getLocationInformation];
//    }

}
-(void) loadMainBandAndPictures:(NSArray *) tweets category:(NSString *) category
{
    CGFloat y = MB_GAP;
    for (UIView *view in sv.subviews) {
        [view removeFromSuperview];
    }
    
    for (NSDictionary *dict in tweets) {
        NSString *text = [dict objectForKey:@"text"];
        if ([dict objectForKey:@"retweeted_status"]) {
            NSDictionary *retweet = [dict objectForKey:@"retweeted_status"];
            text = [retweet objectForKey:@"text"];
        }
        UIImage *tweetImage = nil;
        if ([dict objectForKey:@"extended_entities"]) {
            NSArray *media = [[dict objectForKey:@"extended_entities"] objectForKey:@"media"];
            if (media) {
                if (media.count >0) {
                    NSString *urlImage = [[media objectAtIndex:0] objectForKey:@"media_url"];
                    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlImage]];
                    if (imageData) {
                        tweetImage = [UIImage imageWithData:imageData];
                    }

                }
            }
        }
        NSString *url = @"";
        if ([dict objectForKey:@"id"]) {
                    url = [NSString stringWithFormat:@"https://twitter.com/statuses/%@", [dict objectForKey:@"id"]];
        }
        
        NSString *date =  @"";
        if ([dict objectForKey:@"created_at"]) {
            NSDate *createdDate = [self stringToDate:[dict objectForKey:@"created_at"]];
            if (createdDate) {
                date = [self dateToString:createdDate];
            }
        }
        NewsModel *model = [[NewsModel alloc]
                            initWithDictionary:@{NM_TITLE:(date) ? date : @"",
                                                 NM_DESC:text, NM_URL:(url) ? url : @""}];
        model.categoryTitle = [[dict objectForKey:@"user"] objectForKey:@"screen_name"];
        model.categoryImage = [[dict objectForKey:@"user"] objectForKey:@"profile_image_url"];
        
        
        MainBandView *card = [[MainBandView alloc] initWithFrame:CGRectMake(MB_GAP, y, self.view.frame.size.width - 2*MB_GAP, 0) andModel:model];
        card.delegate = self;
        card.mainImage = tweetImage;

        [sv addSubview:card];
        y = y + card.frame.size.height+MB_GAP;
        
        
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    sv.contentSize = CGSizeMake(sv.frame.size.width, y);

}

-(void) touchInMainBandView:(MainBandView *)view model:(NewsModel *)model
{
    DetailViewController *detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"detailVC"];
    detailVC.currentModel = model;
    [self.navigationController pushViewController:detailVC animated:YES];
}

-(void) touchInImageView:(MainBandView *)view model:(NewsModel *)model
{
    [Flurry logEvent:@"Show_image"];
    GGFullscreenImageViewController *vc = [[GGFullscreenImageViewController alloc] init];
    vc.liftedImageView = view.ivImage;
    [self presentViewController:vc animated:YES completion:nil];
}


-(void) doShareInMainBandView:(MainBandView *)view model:(NewsModel *)model
{
    shareURl = model.urlString;
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Facebook",
                            @"Google+",
                            nil];
    [popup showInView:self.view];

}

-(void) doLikeInMainBandView:(MainBandView *)view model:(NewsModel *)model
{
    
}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIApplication *safari = [UIApplication sharedApplication];
    if (shareURl) {
        if (![shareURl isEqualToString:@""]) {
            switch (buttonIndex) {
                case 0:
                {
                    NSURL * URL = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"https://www.facebook.com/sharer/sharer.php?u=%@", shareURl]];
                    [safari openURL:URL];
                    break;
                }
                case 1:
                {
                    NSURL * URL = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"https://plus.google.com/share?url=%@", shareURl]];
                    [safari openURL:URL];
                    break;
                }
                default:
                    break;
            }
        }
    }
}


-(void) showCategoriesView:(UIBarButtonItem *) sender
{
    CategoryViewController *rightSideMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"rightSideMenuViewController"];
    [self.navigationController pushViewController:rightSideMenuViewController animated:YES];

}


-(void) getTweets:(NSString*)screenName
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    STTwitterAPI *twitter = [STTwitterAPI twitterAPIAppOnlyWithConsumerKey:@"pHeGhjI3ZzBYFuE0aJTBw2jjy"
                                        consumerSecret:@"Jlvvtc6QJktDDoMKQsXRZKwiRiPI0cIaPvoFC2eJ6xrnLtTVPB"];
    
    
    [twitter verifyCredentialsWithUserSuccessBlock:^(NSString *username, NSString *userID) {
        [Flurry logEvent:@"Success_verify_credential"];
        [twitter getUserTimelineWithScreenName:screenName
                                  successBlock:^(NSArray *statuses) {
                                      [mainArray addObjectsFromArray:statuses];
                                      [Flurry logEvent:@"Success_load_tweets"];
                                      [self loadMainBandAndPictures:mainArray category:screenName];
                                  } errorBlock:^(NSError *error) {
                                      [Flurry logEvent:@"Error_load_tweets"];
                                      [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                  }];
        
    } errorBlock:^(NSError *error) {
        [Flurry logEvent:@"Error_verify_credential"];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];        
    }];
    
}


-(NSDate*) stringToDate:(NSString*) string
{
    // convert to date
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    // ignore +11 and use timezone name instead of seconds from gmt
    [dateFormat setDateFormat:@"EEE MMM dd HH:mm:ss zzzz yyyy"];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    NSDate *dte = [dateFormat dateFromString:string];
    return dte;
}


-(NSString *) dateToString:(NSDate *) date
{
    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
    [dateFormat2 setDateFormat:@"MM/dd/YYYY"];
    [dateFormat2 setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *dateString = [dateFormat2 stringFromDate:date];
    return dateString;
}
@end
