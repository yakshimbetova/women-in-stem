//
//  DetailViewController.m
//  Women in STEM
//
//  Created by Macbook on 10/6/15.
//  Copyright © 2015 Techwomen. All rights reserved.
//
#import "Defines.h"
#import "DetailViewController.h"
#import "LoremIpsum.h"
#import "Flurry.h"
#import "GGFullScreenImageViewController.h"
#define D_GAP                5.0
#define BTN_SIZE            44.0
@implementation DetailViewController
@synthesize currentModel;
@synthesize sv;
-(void) viewDidLoad
{
    [super viewDidLoad];

    if (currentModel) {
        self.title = currentModel.title;
    }

    [self loadinterface];
}
-(void) viewDidAppear:(BOOL)animated{
    [Flurry logEvent:@"Detail_view" withParameters:@{@"tweet":currentModel.desc}];
}

-(void) loadinterface
{
    CGFloat y = D_GAP;
    CGFloat w = self.view.frame.size.width;
    if (currentModel)
    {
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(D_GAP, y, w - 2*D_GAP, 10)];
        lblTitle.backgroundColor = [UIColor clearColor];
        lblTitle.textColor = Rgb2UIColor(6, 43, 89);
        lblTitle.font = FONT_TITLE;
        lblTitle.numberOfLines = 0;
        lblTitle.text = currentModel.title;
        [lblTitle sizeToFit];
        [sv addSubview:lblTitle];
        y = CGRectGetMaxY(lblTitle.frame) + D_GAP;
        if (currentModel.image) {
            ivImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, y, w , currentModel.image.size.height*w/currentModel.image.size.width)];
            ivImage.backgroundColor = [UIColor clearColor];
            ivImage.userInteractionEnabled = YES;
            ivImage.contentMode = UIViewContentModeScaleAspectFit;
            ivImage.image = currentModel.image;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showImage)];
            tap.delegate = self;
            [tap setNumberOfTouchesRequired:1];
            [ivImage addGestureRecognizer:tap];
            [sv addSubview:ivImage];
            y = CGRectGetMaxY(ivImage.frame) + D_GAP;
        }

        lblDescription = [[UITextView alloc] initWithFrame:CGRectMake(D_GAP, y, self.view.frame.size.width - 2*D_GAP, 10)];
        lblDescription.backgroundColor = [UIColor clearColor];
        lblDescription.textColor = [UIColor blackColor];
        lblDescription.editable = NO;
        lblDescription.dataDetectorTypes = UIDataDetectorTypeLink;
        lblDescription.font = FONT_DESCRIPTION;
        lblDescription.text = currentModel.desc;
        [lblDescription sizeToFit];
        [sv addSubview:lblDescription];
        y = CGRectGetMaxY(lblDescription.frame) + D_GAP;
        
        // share and like part
        btnShare = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/4.0 - BTN_SIZE/2.0, CGRectGetMaxY(lblDescription.frame) + D_GAP, BTN_SIZE, BTN_SIZE)];
        btnShare.backgroundColor = [UIColor clearColor];
        [btnShare setImage:[UIImage imageNamed:@"ic_share"] forState:UIControlStateNormal];
        [btnShare addTarget:self action:@selector(doShare:) forControlEvents:UIControlEventTouchUpInside];
        [sv addSubview:btnShare];
        
        btnLike = [[UIButton alloc] initWithFrame:CGRectMake(3.0*self.view.frame.size.width/4.0 - BTN_SIZE/2.0, CGRectGetMaxY(lblDescription.frame) + D_GAP, BTN_SIZE, BTN_SIZE)];
        btnLike.backgroundColor = [UIColor clearColor];
        [btnLike setImage:[UIImage imageNamed:@"ic_like"] forState:UIControlStateNormal];
        [btnLike addTarget:self action:@selector(doLike:) forControlEvents:UIControlEventTouchUpInside];
        [sv addSubview:btnLike];
        
        y = CGRectGetMaxY(btnLike.frame) + D_GAP;

    }
    sv.contentSize = CGSizeMake(sv.frame.size.width, y);

}

-(void) showImage
{
    [Flurry logEvent:@"Show_image"];
    GGFullscreenImageViewController *vc = [[GGFullscreenImageViewController alloc] init];
    vc.liftedImageView = ivImage;
    [self presentViewController:vc animated:YES completion:nil];
    
}


-(void) doShare:(UIButton*) sender
{
    [Flurry logEvent:@"do_share_detail_view"];
    shareURl = currentModel.urlString;
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Facebook",
                            @"Google+",
                            nil];
    [popup showInView:self.view];

}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIApplication *safari = [UIApplication sharedApplication];
    if (shareURl) {
        if (![shareURl isEqualToString:@""]) {
            switch (buttonIndex) {
                case 0:
                {
                    NSURL * URL = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"https://www.facebook.com/sharer/sharer.php?u=%@", shareURl]];
                    [safari openURL:URL];
                    break;
                }
                case 1:
                {
                    NSURL * URL = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"https://plus.google.com/share?url=%@", shareURl]];
                    [safari openURL:URL];
                    break;
                }
                default:
                    break;
            }
        }
    }
}


-(void) doLike:(UIButton*) sender
{
    [Flurry logEvent:@"do_like_detail_view"];    
}

@end
