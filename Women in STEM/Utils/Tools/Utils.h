//
//  Utils.h
//  TDAStocks
//
//  Created by Vit Maslov on 25/08/2008.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSString+StringUtils.h"


// debug macro. Use GCC_PREPROCESSOR_DEFINITIONS build flag to define this macro
#ifdef DEBUG
#define DEBUGLOG(a)                 NSLog(a)
#define DEBUGLOG2(a, b)             NSLog(a, b)
#define DEBUGLOG3(a, b, c)          NSLog(a, b, c)
#define DEBUGLOG4(a, b, c, d)       NSLog(a, b, c, d)
#define DEBUGLOG5(a, b, c, d, e)		NSLog(a, b, c, d, e)
#else
#define DEBUGLOG(a)                 ;
#define DEBUGLOG2(a, b)             ;
#define DEBUGLOG3(a, b, c)          ;
#define DEBUGLOG4(a, b, c, d)       ;
#define DEBUGLOG5(a, b, c, d, e)		;
#endif


// macro
#define SIGN(x)		(x < 0 ? -1 : x > 0 ? 1 : 0)



// NSURLRequest override to allow bad certificates
@interface NSURLRequest (NSURLRequestWithIgnoreSSL)

+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString*)host;

@end


//--------------------------------------
@interface Utils : NSObject {	

}

CGRect CGRectResize(CGRect r, CGFloat dx, CGFloat dy);

+ (BOOL)isMultitaskingSupported;
+ (BOOL)isHiResDevice;
+ (BOOL)isIpadDevice;

// parse date from HTTP headers
+ (NSDate *)parseHTTPDateFromString:(NSString *)stringDate;

+ (NSString *)systemPreferredLanguage;
// show alert to the user
+ (void)ShowAlertWithTitle:(NSString *)title message:(NSString *)message;

// get application version from config file
+ (NSString *)getMainBundleVersion:(BOOL)versionOnly;

// parse values from strings
+ (NSInteger)parseIntegerFromString:(NSString *)string;			// parse integer from string
+ (int)parseIntFromString:(NSString *)string;
+ (int64_t)parseLongFromString:(NSString *)string;							// parse long from string
+ (BOOL)parseBOOLFromString:(NSString *)string;							// parse BOOL from string
+ (BOOL)isDigit:(unichar)uChar;
+ (double)parseDoubleFromString:(NSString *)string;				  // parse double from string
+ (NSString *)getShortDateFromTDADateTime:(NSString *)dateTimeString withTime:(BOOL)withTime;
+ (NSString *)getDateFromYYYYMMDDString:(NSString *)dateString;
+ (NSString *)getShortDateFromFOREXDateTime:(NSString *)dateTimeString withTime:(BOOL)withTime;

// URL
+ (NSString *)urlEscapeString:(NSString *) string;
+ (NSString *)urlUnescapeString:(NSString *) string;
+ (NSString *)dataEscapeString:(NSString *) string;

// XML related

// binary data reading
int16_t ReadInt16FromArray(unsigned char *array, size_t offset);
uint16_t ReadUInt16FromArray(unsigned char *array, size_t offset);
int32_t ReadInt32FromArray(unsigned char *array, size_t offset);
int64_t ReadInt64FromArray(unsigned char *array, size_t offset);
Float32 ReadFloat32FromArray(unsigned char *array, size_t offset);
Float64 ReadFloat64FromArray(unsigned char *array, size_t offset);
NSString *ReadNSStingFromArray(unsigned char *array, size_t offset);

+ (NSString *)htmlColorFromUIColor:(UIColor *)color;

+ (int)detectConnectionTypeToHostname:(const char *)host_name;
+ (int)detectConnectionTypeToHost:(const char *)host_addr_str;


+ (int)compareVersion:(NSString *)version1 version2:(NSString *)version2;
@end

