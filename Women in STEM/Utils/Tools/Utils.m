//
//  Utils.m
//  TDAStocks
//
//  Created by Vit Maslov on 25/08/2008.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "Utils.h"
#import "Regex_h.h"
#import <SystemConfiguration/SCNetworkReachability.h>
#import "NSString+StringUtils.h"
//#import "TDAStocksAppDelegate.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


// title label font
#define TITLE_FONT_NAME			@"Helvetica-Bold"
#define TITLE_FONT_SIZE			16


#if __has_feature(objc_arc)
// use simple bridging transfer
#define         NSSTR_TO_CFSTR(s)       ((__bridge CFStringRef) s)
#define         CFSTR_TO_NSSTR(s)       ((__bridge NSString *) s)
#else
#define         NSSTR_TO_CFSTR(s)       ((CFStringRef) s)
#define         CFSTR_TO_NSSTR(s)       ((NSString *) s)
#endif




//**************************************************************************************//
@implementation NSURLRequest (NSURLRequestWithIgnoreSSL)

+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString*)host{
	return YES;
}

@end



//**************************************************************************************//
@implementation Utils


//
CGRect CGRectResize(CGRect r, CGFloat dx, CGFloat dy){
  return CGRectMake(r.origin.x, r.origin.y, r.size.width+dx, r.size.height+dy);
}

//
static BOOL multitaskingSupported = NO;
static BOOL isiPadDevice = NO;
static BOOL isHiResScreen = NO;

// determine if we can handle multitasking
+ (void)initialize{
	UIDevice* device = [UIDevice currentDevice];
	multitaskingSupported = NO;

  // multitasking
	if ([device respondsToSelector:@selector(isMultitaskingSupported)])
		multitaskingSupported = device.multitaskingSupported;
	
  // is iPad device
  if([device respondsToSelector:@selector(userInterfaceIdiom)]){
    isiPadDevice = (device.userInterfaceIdiom == UIUserInterfaceIdiomPad);
  }
  
    
    DEBUGLOG2(@"scale %f", [UIScreen mainScreen].scale);
  // hires screen
  isHiResScreen = ([UIScreen mainScreen].scale > 1.9);

	DEBUGLOG2(@"Multitasking %@", multitaskingSupported ? @"ENABLED" : @"DISABLED");
}

//
+ (BOOL)isMultitaskingSupported{
	return multitaskingSupported;
}


// is HiRes device
+ (BOOL)isHiResDevice{
  return isHiResScreen;
}


// is iPad
+ (BOOL)isIpadDevice{
  return isiPadDevice;
}

// parse RFC date into NSDate
+ (NSDate *)parseHTTPDateFromString:(NSString *)stringDate{
	NSDate *date = nil;
	
	@try {
		if(stringDate){
			NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
			NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
			if(locale) [dateFormatter setLocale:locale];
			[dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss zzz"];
			date = [dateFormatter dateFromString:stringDate];
			if(!date){
				[dateFormatter setDateFormat:@"dd MMM yyyy HH:mm:ss zzz"];
				date = [dateFormatter dateFromString:stringDate];
			}
		}
	}@catch (NSException * e) {
	}
	
	return date;
}


// get system preferred language
+ (NSString *)systemPreferredLanguage{
	NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
	NSArray* langs = [defs objectForKey:@"AppleLanguages"];
	NSString* preferredLang = (langs  &&  [langs count] > 0) ? [langs objectAtIndex:0] : nil;
	return preferredLang;
}

/*!
  Initialization
 */
//+ (void) initialize{ 
//	static BOOL initialized = NO; 
//	if (!initialized) { 
//		// check what is a decimal delimiter
//		... 
//		initialized = YES; 
//	} 
//} 
//

/**********************************************************************/
/*!
  Show alert to the user
 */
+ (void)ShowAlertWithTitle:(NSString *)title message:(NSString *)message{
	UIAlertView *aview = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	
	[aview show];
  
#if ! __has_feature(objc_arc)
	[aview release];
#endif
}


// get application version from config file
+ (NSString *)getMainBundleVersion:(BOOL)versionOnly{
	NSString *ver = (NSString *) [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
	ver = ver ? ver : @"1.0.0";
	return versionOnly ? ver : [NSString stringWithFormat:NSLocalizedString(@"Version %@", @"Application version on About page"), ver];
}

/**********************************************************************/
/*!
 Parse integer from string
 */
+ (NSInteger)parseIntegerFromString:(NSString *)string{
	NSInteger value = 0;
	
	@try{
		value = [string integerValue];
	}@catch(id ue){
		value = 0;
	}
	
	return value;
}


+ (int)parseIntFromString:(NSString *)string{
	int value = 0;
	
	@try{
		value = [string intValue];
	}@catch(id ue){
		value = 0;
	}
	
	return value;
}


/*!
 Parse integer from string
 */
+ (int64_t)parseLongFromString:(NSString *)string{
	int64_t value = 0;
	
	@try{
		value = [string longLongValue];
	}@catch(id ue){
		value = 0;
	}
	
	return value;
}


/*!
  Parse BOOL value from string
 */
+ (BOOL)parseBOOLFromString:(NSString *)string{
	BOOL result = NO;
	
	@try{
    // check for "on"/"off"
    if(string  &&  [string isKindOfClass:[NSString class]]){
      if([string caseInsensitiveCompare:@"on"] == NSOrderedSame)
        return YES;
      else if([string caseInsensitiveCompare:@"off"] == NSOrderedSame)
        return NO;
    }
		// Returns YES on encountering one of "Y", "y", "T", "t", or a digit 1-9—the method ignores any trailing characters.
		result = [string boolValue];
	}@catch(id ue){
	}
	
	return result;
}


// check if uChar is digit
+ (BOOL)isDigit:(unichar)uChar{
	switch(uChar){
		case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
			return YES;
			break;
	}
	return NO;
}

/*!
 Parse double value from string
 */
+ (double)parseDoubleFromString:(NSString *)string{
	double value = 0.0;
	
	if([NSString isNullOrEmpty:string]) return value;
	NSMutableString *ms = [NSMutableString stringWithString:string];
	
	@try{
		// check for string format and set "xx.xxEy" to IEEE standart "x.xxxxE+yy".
		if(![NSString isNullOrEmpty:string]){
			NSRange rng = [ms rangeOfString:@"E" options:NSCaseInsensitiveSearch];
			if(rng.location != NSNotFound  &&  rng.location+1 < [ms length]){		// we must also must have at least one next digit to 'E'
				// navigate to sign position
				rng.location++;
				// check for sign after 'E' and before digit
				if([Utils isDigit:[ms characterAtIndex:rng.location]])
					[ms insertString:@"+" atIndex:rng.location];
				// check sign and if sign is present check if there is two or more digits after sign
				switch([ms characterAtIndex:rng.location]){
					case '+': case '-':
						// skip to digit after sign
						rng.location++;
						if(rng.location < [ms length])
							if([Utils isDigit:[ms characterAtIndex:rng.location]]){	// first symbol is digit, check for next
								if(rng.location+1 < [ms length]  &&  [Utils isDigit:[ms characterAtIndex:rng.location+1]]){
									// next symbol present and it is a digit, do nothing as far as it is Ok
									;
								}else{
									// no next symbol present or second symbol is not a digit, add '0' after sign
									[ms insertString:@"0" atIndex:rng.location];
								}
              }
						break;
				}
			}
		}
		// get value
		value = [ms doubleValue];
	}@catch(id ue){
		value = 0.0;
	}
	
	return value;
}


// get date part from TDA date-time string. Date-time is in format "2007-03-23 15:09:59 EDT"
// TDA NOTE: Timezone may be EDT or EST for now
+ (NSString *)getShortDateFromTDADateTime:(NSString *)dateTimeString withTime:(BOOL)withTime{
	NSRange rng;

	if(withTime){
		if([dateTimeString length] < 16) return @"No date";
		rng.location = 0; rng.length = 16;
	}else{
		if([dateTimeString length] < 10) return @"No date";
		rng.location = 0; rng.length = 10;
	}
	
	return [[dateTimeString stringByReplacingOccurrencesOfString:@"T" withString:@" "] substringWithRange:rng];
	
//	int year, month, day;
//	// year
//	rng.location = 0; rng.length = 4;
//	year = [Utils parseIntegerFromString:[dateTimeString substringWithRange:rng]];
//	if(year < 1900  ||  year > 4000) year = 0;
//	// month
//	rng.location = 5; rng.length = 2;
//	month = [Utils parseIntegerFromString:[dateTimeString substringWithRange:rng]];
//	if(month < 1  ||  month > 12) month = 0;
//	// day
//	rng.location = 8; rng.length = 2;
//	day = [Utils parseIntegerFromString:[dateTimeString substringWithRange:rng]];
//	if(day < 1  ||  day > 31) day = 0;
	
}

// get date part from FOREX.com date-time string. Date-time is in format "2007-03-23 15:09:59" or "2007-03-23T15:09:59-05:00"
// omiting TimeZone
+ (NSString *)getShortDateFromFOREXDateTime:(NSString *)dateTimeString withTime:(BOOL)withTime{
	NSRange rng;
	
	if(withTime){
		if([dateTimeString length] < 19) return @"No date";
		rng.location = 0; rng.length = 19;
	}else{
		if([dateTimeString length] < 10) return @"No date";
		rng.location = 0; rng.length = 10;
	}
	
	return [[dateTimeString stringByReplacingOccurrencesOfString:@"T" withString:@" "] substringWithRange:rng];
}
	
// get date from YYYYMMDD string and return it as sring date
+ (NSString *)getDateFromYYYYMMDDString:(NSString *)dateString{
	if(!dateString  ||  [dateString length] != 8) return nil;
	
	NSRange rng;
	long year, month, day;
	// year
	rng.location = 0; rng.length = 4;
	year = [Utils parseIntegerFromString:[dateString substringWithRange:rng]];
	if(year < 1900  ||  year > 4000) year = 0;
	// month
	rng.location = 4; rng.length = 2;
	month = [Utils parseIntegerFromString:[dateString substringWithRange:rng]];
	if(month < 1  ||  month > 12) month = 0;
	// day
	rng.location = 6; rng.length = 2;
	day = [Utils parseIntegerFromString:[dateString substringWithRange:rng]];
	if(day < 1  ||  day > 31) day = 0;
	
	return [NSString stringWithFormat:@"%ld-%ld-%ld", year, month, day];
}


/**********************************************************************/
// URL Escape string
+ (NSString *)urlEscapeString:(NSString *)string{
    if (!string) {
        return @"";
    }
	CFStringRef escapeChars = CFSTR("+");
	CFStringRef escapedString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, NSSTR_TO_CFSTR(string), NULL, escapeChars, kCFStringEncodingUTF8);
	NSString *newString = [NSString stringWithString:CFSTR_TO_NSSTR(escapedString)];
	// as far as returned string is retained we must release it
	CFRelease(escapedString);
	return newString;
}

// URL unescape string
+ (NSString *)urlUnescapeString:(NSString *)string{
    if (!string) {
        return @"";
    }
	CFStringRef unescapedString = CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault, NSSTR_TO_CFSTR(string), CFSTR(""), kCFStringEncodingUTF8);
	NSString *newString = [NSString stringWithString:CFSTR_TO_NSSTR(unescapedString)];
	// as far as returned string is retained we must release it
	CFRelease(unescapedString);
	return newString;
}

// Data Escape string
+ (NSString *)dataEscapeString:(NSString *)string{
	if([NSString isNullOrEmpty:string]) return @"";
	CFStringRef escapeChars = (CFStringRef) @"+=&:,/|@?! ";
	CFStringRef escapedString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, NSSTR_TO_CFSTR(string), NULL, escapeChars, kCFStringEncodingUTF8);
	NSString *newString = [NSString stringWithString:CFSTR_TO_NSSTR(escapedString)];
	// as far as returned string is retained we must release it
	CFRelease(escapedString);
	return newString;
}




/*********************************************************************/

// return 16bit integer formed from array
int16_t ReadInt16FromArray(unsigned char *array, size_t offset){
	// form little-endian value
	uint16_t value = (uint16_t) array[offset] << 8;
	value += (uint16_t) array[offset+1];
	// return value converted to host's byte order
	value = CFSwapInt16LittleToHost(value);
	return value;
}

// return 16bit integer formed from array
uint16_t ReadUInt16FromArray(unsigned char *array, size_t offset){
	// form little-endian value
	uint16_t value = (uint16_t) array[offset] << 8;
	value += (uint16_t) array[offset+1];
	// return value converted to host's byte order
	value = CFSwapInt16LittleToHost(value);
	return value;
}


// return 32bit integer formed from array
int32_t ReadInt32FromArray(unsigned char *array, size_t offset){
	// form little-endian value
	uint32_t value = (uint32_t) array[offset] << 24;
	value += (uint32_t) array[offset+1] << 16;
	value += (uint32_t) array[offset+2] << 8;
	value += (uint32_t) array[offset+3];
	// return value converted to host's byte order
	return CFSwapInt32LittleToHost(value);
}


// return 64bit integer formed from array
int64_t ReadInt64FromArray(unsigned char *array, size_t offset){
	// form little-endian value
	uint64_t value = (uint64_t) array[offset] << 56;
	value += (uint64_t) array[offset+1] << 48;
	value += (uint64_t) array[offset+2] << 40;
	value += (uint64_t) array[offset+3] << 32;
	value += (uint64_t) array[offset+4] << 24;
	value += (uint64_t) array[offset+5] << 16;
	value += (uint64_t) array[offset+6] << 8;
	value += (uint64_t) array[offset+7];
	// return value converted to host's byte order
	return CFSwapInt64LittleToHost(value);
}


// return float (32bit) formed from array
Float32 ReadFloat32FromArray(unsigned char *array, size_t offset){
	// form little-endian value
	CFSwappedFloat32 value;
	value.v = (uint32_t) array[offset];
	value.v += (uint32_t) array[offset+1] << 8;
	value.v += (uint32_t) array[offset+2] << 16;
	value.v += (uint32_t) array[offset+3] << 24;
	// return value converted to host's byte order
	return CFConvertFloat32SwappedToHost(value);
}


// return float (64bit) integer formed from array
Float64 ReadFloat64FromArray(unsigned char *array, size_t offset){
	// form little-endian value
	CFSwappedFloat64 value;
	value.v = (uint64_t) array[offset];
	value.v += (uint64_t) array[offset+1] << 8;
	value.v += (uint64_t) array[offset+2] << 16;
	value.v += (uint64_t) array[offset+3] << 24;
	value.v += (uint64_t) array[offset+4] << 32;
	value.v += (uint64_t) array[offset+5] << 40;
	value.v += (uint64_t) array[offset+6] << 48;
	value.v += (uint64_t) array[offset+7] << 56;
	// return value converted to host's byte order
	return CFConvertFloat64SwappedToHost(value);
}


// return string formed from array
NSString *ReadNSStingFromArray(unsigned char *array, size_t offset){
	size_t len = ReadInt16FromArray(array, offset);
	if(len < 1) return @"";
	
#if __has_feature(objc_arc)
	return [[NSString alloc] initWithBytes:(void *)&array[offset+2] length:len encoding:NSASCIIStringEncoding];
#else
	return [[[NSString alloc] initWithBytes:(void *)&array[offset+2] length:len encoding:NSASCIIStringEncoding] autorelease];
#endif
}



// create HTML color from UIColor
+ (NSString *)htmlColorFromUIColor:(UIColor *)color{
	size_t numComponents = CGColorGetNumberOfComponents(color.CGColor);
	if(numComponents == 4){
		const CGFloat *components = CGColorGetComponents(color.CGColor);
		return [NSString stringWithFormat:@"#%02x%02x%02x", (int) (components[0]*0xFF), (int) (components[1]*0xFF), (int) (components[2]*0xFF)];
	}else{
		return @"#000000";
	}
}


/**********************************************************************/
// ... determine connetcion type. Returns:
// 0 - no connection
// 1 - 3G/EDGE/GPRS
// 2 - Wi-Fi or other connection
+ (int)detectConnectionTypeToHostname:(const char *)host_name{
	Boolean success;    
	
	SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL, host_name);
	SCNetworkReachabilityFlags flags;
	success = SCNetworkReachabilityGetFlags(reachability, &flags);
	success = success && (flags & kSCNetworkReachabilityFlagsReachable) && !(flags & kSCNetworkReachabilityFlagsConnectionRequired);
	
	if(success){
		if(flags & kSCNetworkReachabilityFlagsIsWWAN)
			return 1;
		else
			return 2;
  }
	
	return 0;
}
//
+ (int)detectConnectionTypeToHost:(const char *)host_addr_str{
	Boolean success;
	
	//SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL, host_name);
  struct sockaddr_in sin;
  bzero(&sin, sizeof(sin));
  sin.sin_len = sizeof(sin);
  sin.sin_family = AF_INET;
  inet_aton(host_addr_str, &sin.sin_addr);
  
  SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&sin);
	SCNetworkReachabilityFlags flags;
	success = SCNetworkReachabilityGetFlags(reachability, &flags);
	success = success && (flags & kSCNetworkReachabilityFlagsReachable) && !(flags & kSCNetworkReachabilityFlagsConnectionRequired);
	
	if(success){
		if(flags & kSCNetworkReachabilityFlagsIsWWAN)
			return 1;
		else
			return 2;
  }
	
	return 0;
}


+ (int)compareVersion:(NSString *)version1 version2:(NSString *)version2{
    const int MAX_PARTS = 4;
    NSString *s1 = version1 ? version1 : @"";
    NSString *s2 = version2 ? version2 : @"";
    
    NSMutableArray *v1 = [[s1 componentsSeparatedByString:@"."] mutableCopy];
    NSMutableArray *v2 = [[s2 componentsSeparatedByString:@"."] mutableCopy];
    
    while(v1.count < MAX_PARTS) [v1 addObject:@"0"];
    while(v2.count < MAX_PARTS) [v2 addObject:@"0"];
    
    int i=0;
    while (i < MAX_PARTS) {
        int value1 = [[v1 objectAtIndex:i] intValue];
        int value2 = [[v2 objectAtIndex:i] intValue];
        if(value1 > value2)
            return 1;
        if(value2 > value1)
            return -1;
        i++;
    }
    return 0;
}



@end


