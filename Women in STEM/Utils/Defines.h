//
//  Defines.h
//  Women in STEM
//
//  Created by Macbook on 10/5/15.
//  Copyright © 2015 Techwomen. All rights reserved.
//

#ifndef Defines_h
#define Defines_h

#define FLURRY_KEY @"FTB9P92XM4JVRD6DDHPZ"
#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]
#define FONT_TITLE          [UIFont boldSystemFontOfSize:13.0]
#define FONT_DESCRIPTION    [UIFont systemFontOfSize:15.0]

#define TECHWOMEN_TW    @"techwomen"
#define TECHGIRLS_TW    @"techgirls"
#endif /* Defines_h */
