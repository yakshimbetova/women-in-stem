//
//  AppSettings.m
//

#import "AppSettings.h"
#import "Utils.h"
#import "NSString+StringUtils.h"



static NSString *defaultKeyTechwomen = @"DefTechwomen";
static NSString *defaultKeyTechGirls = @"DefTechGirls";

// predefined keys
#pragma mark --- AppSettings class ---
@implementation AppSettings
@synthesize userPrefsKey;
@dynamic techgirls, techwomen;
static AppSettings *_appSettings = nil;
//
+ (AppSettings *)applicationSettings{
    if(!_appSettings){
        // get default key
        NSString *defaultsKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        if(defaultsKey == nil)
            defaultsKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"];
        
        if(defaultsKey == nil)
            defaultsKey = @"DefaultPrefs";
        
        _appSettings = [[AppSettings alloc] initWithKey:defaultsKey appDefaultPrefs:nil];
    }
    return _appSettings;
}

// init
- (id)initWithKey:(NSString *)prefsKey appDefaultPrefs:(NSDictionary *)appDefaultPrefs{
    if(self = [super init]){
        
        // dictionary key
        self.userPrefsKey = [NSString isNullOrEmpty:prefsKey] ? @"DefaultPrefs" : prefsKey;
        // register defaults
        [self userPrefsRegisterDefaults:appDefaultPrefs];
        
        //preferences = nil;
        // load preferences
        [self loadSettings];
    }
    return self;
}

// dealloc
- (void)dealloc{
}


#pragma mark --- Load, save, defaults ---
//  Set default user preferences
- (void)userPrefsRegisterDefaults:(NSDictionary *)appDefaults{
    // dictionary with defaults
    NSMutableDictionary *defPrefs = appDefaults ? [NSMutableDictionary dictionaryWithDictionary:appDefaults] : [NSMutableDictionary dictionary];
    
    if(![defPrefs objectForKey:defaultKeyTechwomen]){
        [defPrefs setValue:@"YES" forKey:defaultKeyTechwomen];
    }
    
    if(![defPrefs objectForKey:defaultKeyTechGirls]){
        [defPrefs setValue:@"NO" forKey:defaultKeyTechGirls];
    }

    // NSRegistrationDomain dictionary
    NSDictionary *domainDict = [NSDictionary dictionaryWithObject:defPrefs forKey:userPrefsKey];
    [[NSUserDefaults standardUserDefaults] registerDefaults:domainDict];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

// Load user preferences
- (BOOL)loadSettings{
    // set directly as far as we use COPY method and do not need to retain dictionary.
    preferences = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:userPrefsKey] mutableCopy];
    
    // *** test *** //
    //for(NSString *s in userPreferences) NSLog(@"Key: %@, Value: %@", s, [userPreferences objectForKey:s]);
    
    return preferences != nil;
}

// Save user preferences
- (void)saveSettings{
    if(preferences){
        // save prefs
        [[NSUserDefaults standardUserDefaults] setObject:preferences forKey:userPrefsKey];
    }
}

- (NSString *)getStringValue:(NSString *)key{
    NSString *s = nil;
    
    @try{
        s = (NSString *)[self getValueForKey:key];
        if(![NSString isNullOrEmpty:s]) s = [NSString stringDecode:s];
    }@catch(id){
        s = nil;
    }
    
    return [NSString stringWithString:([NSString isNullOrEmpty:s] ? @"" : s)];
}

// any object
- (id)getValueForKey:(NSString *)key{
    id o = nil;
    
    @try{
        if(preferences) o = [preferences objectForKey:key];
    }@catch(id){
        o = nil;
    }
    
    return o;
}


// BOOL value
- (BOOL)getBOOLValue:(NSString *)key{
    return [Utils parseBOOLFromString:[self getStringValue:key]];
}
//
- (void)setBOOLValue:(BOOL)value forKey:(NSString *)key{
    [self setStringValue:[NSString stringWithFormat:@"%d", value] forKey:key];
}

- (void)setStringValue:(NSString *)value forKey:(NSString *)key{
    [self setValue:([NSString isNullOrEmpty:value] ? value : [NSString stringEncode:value]) forKey:key];
}


- (void)setValue:(id)value forKey:(NSString *)key{
    @try{
        if(preferences) [preferences setValue:(value) forKey:key];
    }@catch(id){ }
    
}

// techwomen
- (BOOL) techwomen{
    return [self getBOOLValue:defaultKeyTechwomen];
}
//
- (void)setTechwomen:(BOOL)techwomen
{
    [self setBOOLValue:techwomen forKey:defaultKeyTechwomen];
}

// techgirls
- (BOOL) techgirls{
    return [self getBOOLValue:defaultKeyTechGirls];
}
//
-(void) setTechgirls:(BOOL)techgirls
{
    [self setBOOLValue:techgirls forKey:defaultKeyTechGirls];
}

@end
