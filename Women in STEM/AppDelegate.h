//
//  AppDelegate.h
//  Women in STEM
//
//  Created by Macbook on 10/5/15.
//  Copyright © 2015 Techwomen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"
#import "Flurry.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    MFSideMenuContainerViewController *container;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MFSideMenuContainerViewController *container;

@end

